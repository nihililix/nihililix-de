# nihililix.de
nihililix.de

[![pipeline status](https://gitlab.com/nihililix/nihililix-de/badges/master/pipeline.svg)](https://gitlab.com/nihililix/nihililix-de/-/commits/master)

## Contents
1. Quickstart
2. Install
3. Frontend
4. Features / Dependencies

## Quickstart

- Install `git clone --recursive https://gitlab.com/nihililix/nihililixde.git` the repository
- install dependencies `yarn`
- build frontend `yarn build`
- or serve with `yarn serve`

## Install

##### Installation 
Install default files and lynnara-kit submodules
```
git clone --recursive https://gitlab.com/nihililix/nihililixde.git
```

## Frontend

##### Installation
```
yarn
```

##### Build Prod Version
```
yarn build
```

##### Build Dev Version
```
yarn serve
```

## Features / Dependencies
* scss
* es6
* webpack
* stylelint
* eslint
* browserlist
* lynnara-kit
