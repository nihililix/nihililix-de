/**
 * Stylelint Config
 */
/* eslint-disable */
module.exports = {
    'plugins': [
        'stylelint-scss',
        'stylelint-order'
    ],
    'extends': [
        './config/stylelint.default.js',
        './config/stylelint.scss.js',
        './config/stylelint.order.js'
    ],
    'ignoreFiles': [
        // Enter Files and Folders for ignore
        '**/node_modules/**',
        '**/*.*',
        '!**/*.scss',
    ],
    'defaultSeverity': 'warning',
    'rules': {
        'indentation': 4
    }
};
/* eslint-enable */
