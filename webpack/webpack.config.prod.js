const Webpack = require('webpack');
const merge = require('webpack-merge');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const common = require('./webpack.common.js');
const purgecss = require('@fullhuman/postcss-purgecss')

module.exports = merge(common, {
    mode: 'production',
    devtool: 'source-map',
    stats: 'errors-warnings',
    bail: true,
    output: {
        filename: 'js/[name].js',
        chunkFilename: 'js/[name].[chunkhash:8].chunk.js'
    },
    plugins: [
        new Webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        }),
        new MiniCssExtractPlugin({
            filename: 'bundle.css'
        }),
        new Webpack.optimize.ModuleConcatenationPlugin(),
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: 'babel-loader'
            },
            {
                test: /\.s?css/i,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },

                    'css-loader?sourceMap=true',
                    {
                        loader: 'postcss-loader',
                        options: {
                            postcssOptions: {

                                ident: 'postcss',
                                plugins: [
                                    require('autoprefixer')({}),
                                    purgecss({
                                        content: ['./**/*.hbs'],
                                        safelist: [/.*@xs$/, /.*@sm$/, /.*@md$/, /.*@lg$/, /.*@xl$/, 'konamicode']
                                    })
                                ]
                            }
                        }
                    },
                    'sass-loader'
                ]
            }
        ]
    }
});
