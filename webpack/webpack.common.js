const webpack = require('webpack');
const Path = require('path');
const autoprefixer = require('autoprefixer');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const CssnanoPlugin = require('cssnano-webpack-plugin');
const { SitemapPlugin } = require('@jahed/webpack-sitemap');

module.exports = {
    entry: {
        app: Path.resolve(__dirname, '../src/scripts/index.js'),
    },
    output: {
        path: Path.join(__dirname, '../www/'),
        filename: 'js/index.js',
    },
    optimization: {
        splitChunks: {
            chunks: 'all',
            name: false,
        },
        minimizer: [
            new CssnanoPlugin()
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new webpack.LoaderOptionsPlugin({
            options: {
                postcss: [
                    autoprefixer()
                ]
            }
        }),
        new CopyWebpackPlugin({
            patterns: [
                { from: 'src/assets', to: 'assets' }
            ]
        }),
        new webpack.LoaderOptionsPlugin({
            options: {
                handlebarsLoader: {}
            }
        }),
        new HtmlWebpackPlugin({
            // template: Path.resolve(__dirname, '../src/index.html'),
            template: Path.resolve(__dirname, '../src/index.twig'),
        }),

        new SitemapPlugin({
            basename: 'https://ferienwohnung-werratal.de',
            sitemapindex: {
                'app': {
                    urlset: {
                        '/': {
                            priority: 1
                        }
                    }
                }
            },
            defaults: {
                sitemap: {
                    lastmod: new Date().toISOString()
                },
                url: {
                    lastmod: new Date().toISOString(),
                    priority: 1,
                    changefreq: 'monthly'
                }
            }
        })

    ],
    resolve: {
        alias: {
            '~': Path.resolve(__dirname, '../src'),
        },
    },
    module: {
        rules: [
            {
                test: /\.mjs$/,
                include: /node_modules/,
                type: 'javascript/auto',
            },
            {
                test: /\.twig$/,
                use: {
                    loader: 'twig-loader',
                    options: {
                    },
                }
            },



            {
                test: /\.(png|jpe?g|webp|git|svg|ico|)$/i,
                use: [
                    {
                        loader: `img-optimize-loader`,
                        options: {
                            name: '[path][name].[ext]',

                            compress: {
                                // This will transform your png/jpg into webp.

                                webp: {
                                    quality: 75,

                                },

                            },


                        },
                    },
                ],
            },


        ],
    },
};
