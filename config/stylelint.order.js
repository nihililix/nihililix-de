/**
 * Stylelint Order config
 */
module.exports = {
  'ignoreFiles': [
    '**/node_modules/**'
  ],
  'defaultSeverity': 'warning',
  'rules': {
    "order/order": [
      "declarations",
      {
        "type": "rule",
        "selector": "^$_"
      },
      {
        "type": "at-rule",
        "name": "media"
      },
      {
        "type": "rule",
        "selector": "^&::(before|after)"
      },
      {
        "type": "rule",
        "selector": "^."
      },
      {
        "type": "rule",
        "selector": "^&:\\w"
      },
      {
        "type": "rule",
        "selector": "^&:*"
      },
      {
        "type": "rule",
        "selector": "^&--"
      },
      {
        "type": "rule",
        "selector": "^&_"
      },
      {
        "type": "rule",
        "selector": "^&__"
      }
    ],
    "order/properties-order": [
      {
        "order": "strict",
        "properties": [
          "$variable",
          "$include",
          "$extend"
        ]
      },
      {
        "order": "strict",
        "properties": [
          "content",
          "box-sizing",
          "display",

          "position",
          "top",
          "right",
          "bottom",
          "left",

          "width",
          "min-width",
          "max-width",
          "height",
          "min-height",
          "max-height",
          "margin",
          "margin-top",
          "margin-right",
          "margin-bottom",
          "margin-left",
          "padding",
          "padding-top",
          "padding-right",
          "padding-bottom",
          "padding-left",
        ]
      },
      {
        "order": "strict",
        "properties": [
          "border",
          "border-collapse",
          "border-width",
          "border-style",
          "border-color",
          "border-top",
          "border-top-width",
          "border-top-style",
          "border-top-color",
          "border-right",
          "border-right-width",
          "border-right-style",
          "border-right-color",
          "border-bottom",
          "border-bottom-width",
          "border-bottom-style",
          "border-bottom-color",
          "border-left",
          "border-left-width",
          "border-left-style",
          "border-left-color",
          "border-radius",
          "border-top-left-radius",
          "border-top-right-radius",
          "border-bottom-right-radius",
          "border-bottom-left-radius",
          "border-image",
          "border-image-source",
          "border-image-slice",
          "border-image-width",
          "border-image-outset",
          "border-image-repeat",
        ]
      },
      {
        "order": "strict",
        "properties": [
          "visibility",
          "float",
          "clear",
          "overflow",
          "overflow-x",
          "overflow-y",
          "clip",
          "zoom",
          "flex",
          "flex-grow",
          "flex-shrink",
          "flex-basis",
          "flex-direction",
          "flex-order",
          "flex-pack",
          "flex-align",
          "flex-wrap",
          "justify-content",
          "align-items"
        ]
      },
      {
        "order": "strict",
        "properties": [
          "background",
          "filter:progid:DXImageTransform.Microsoft.AlphaImageLoader",
          "background-color",
          "background-image",
          "background-repeat",
          "background-attachment",
          "background-position",
          "background-position-x",
          "background-position-y",
          "background-clip",
          "background-origin",
          "background-size",

          "object-fit",
          "object-position",
          "filter:progid:DXImageTransform.Microsoft.Alpha(Opacity",
          "-ms-filter:\\'progid:DXImageTransform.Microsoft.Alpha",
          "-ms-interpolation-mode",

          "outline",
          "outline-width",
          "outline-style",
          "outline-color",
          "outline-offset",
          "box-decoration-break",
          "box-shadow",
          "filter:progid:DXImageTransform.Microsoft.gradient",
          "-ms-filter:\\'progid:DXImageTransform.Microsoft.gradient",

          "text-shadow",
          "color",
        ]
      },
      {
        "order": "strict",
        "properties": [
          "font",
          "font-family",
          "font-size",
          "font-weight",
          "font-style",
          "font-variant",
          "font-size-adjust",
          "font-stretch",
          "font-effect",
          "font-emphasize",
          "font-emphasize-position",
          "font-emphasize-style",
          "font-smooth",
          "line-height",

          "text-align",
          "text-align-last",
          "vertical-align",
          "white-space",
          "text-decoration",
          "text-emphasis",
          "text-emphasis-color",
          "text-emphasis-style",
          "text-emphasis-position",
          "text-indent",
          "text-justify",
          "text-transform",
          "letter-spacing",
          "word-spacing",
          "text-outline",
          "text-transform",
          "text-wrap",
          "text-overflow",
          "text-overflow-ellipsis",
          "text-overflow-mode",
          "word-wrap",
          "word-break",
          "tab-size",
          "hyphens",
        ]
      },
      {
        "order": "strict",
        "emptyLineBefore": "always",
        "properties": [
          "table-layout",
          "empty-cells",
          "caption-side",
          "border-spacing",
          "border-collapse",
          "list-style",
          "list-style-position",
          "list-style-type",
          "list-style-image",

          "quotes",
          "counter-reset",
          "counter-increment",
          "resize",
          "cursor",
          "user-select",
          "nav-index",
          "nav-up",
          "nav-right",
          "nav-down",
          "nav-left",

          "pointer-events",
          "opacity",
        ]
      },
      {
        "order": "strict",
        "properties": [
          "transform",
          "transform-origin",

          "animation",
          "animation-name",
          "animation-duration",
          "animation-play-state",
          "animation-timing-function",
          "animation-delay",
          "animation-iteration-count",
          "animation-direction",

          "transition",
          "transition-delay",
          "transition-timing-function",
          "transition-duration",
          "transition-property",
        ]
      },
      {
        "order": "strict",
        "emptyLineBefore": "always",
        "properties": [
          "z-index",
        ]
      },
      {
        "order": "flexible",
        "emptyLineBefore": "always",
        "properties": [
          "$include breakpoint"
        ]
      }
    ]
  }
}
